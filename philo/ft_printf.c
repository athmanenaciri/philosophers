/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: anaciri <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/07/21 17:55:58 by anaciri           #+#    #+#             */
/*   Updated: 2022/08/23 17:56:15 by anaciri          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include"philo.h"

void	ft_printf(t_philo *philo, char *format)
{
	pthread_mutex_lock(&philo->data->print_lock);
	if (philo->data->stop == 0)
		printf(format, between_times(philo->data->start_time), philo->nbr);
	pthread_mutex_unlock(&philo->data->print_lock);
}
